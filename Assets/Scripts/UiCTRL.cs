using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UiCTRL : MonoBehaviour
{
    public static UiCTRL instance;
    public Recycle recycle;
    public UiAttackLog uiAttackLog;
    public UiMessageBox uiMessageBox;
    public Transform whiteContent;
    public Transform blackContent;







    void Awake() => instance = this;





    public void Init()
    {
        recycle?.Init();
        uiAttackLog?.Init();
        uiMessageBox?.Init();
    }







    public Button GetNewRecycleButton(SinglePieceData pieceData)
    {
        Button btnTemp = new GameObject().AddComponent<Button>();

        switch (pieceData.PlayerID)
        {
            case 0: btnTemp.transform.SetParent(whiteContent, false); break;
            case 1: btnTemp.transform.SetParent(blackContent, false); break;
        }

        Image sprite = btnTemp.AddComponent<Image>();
        sprite.sprite = ResourceManager.instance.GetSpriteByTypeID(pieceData.Type, pieceData.PlayerID);
        btnTemp.name = pieceData.Type.ToString();
        btnTemp.onClick.AddListener(() => { });
        btnTemp.interactable = false;

        return btnTemp;
    }







    public void ClearAllButtons()
    {
        for (int i = 0; i < whiteContent.childCount; i++) Destroy(whiteContent.GetChild(i).gameObject);
        for (int i = 0; i < blackContent.childCount; i++) Destroy(blackContent.GetChild(i).gameObject);
    }





}




