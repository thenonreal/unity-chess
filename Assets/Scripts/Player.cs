using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using UnityEngine;

public class Player : MonoBehaviour
{
    public PlayerData data = new PlayerData();
    public List<Piece> myPieces = new List<Piece>();
    Sprite[] mySpritePieces;









    public void Init(int id)
    {
        data.playerID = id;
        data.colorType = (ColorType)id;
        if (data.colorType == ColorType.White) mySpritePieces = ResourceManager.instance.whitePieces;
        else mySpritePieces = ResourceManager.instance.blackPieces;
        GenerateMyPieces(DataManager.instance.metaData.PiecesData);
    }







    public void GenerateMyPieces(List<PiecesData> piecesData)
    {
        for (int i = 0; i < piecesData.Count; i++)
        {
            List<SinglePieceData> singlesPieces = piecesData[i].Pieces;
            for (int j = 0; j < singlesPieces.Count; j++)
            {
                Piece tempPiece = InstantiateNewPiece(singlesPieces[j], mySpritePieces[i]);
                myPieces.Add(tempPiece);
            }
        }
    }







    Piece InstantiateNewPiece(SinglePieceData pieceData, Sprite pieceSprite)
    {
        Piece piece = new GameObject($"Piece").AddComponent<Piece>();
        piece.Init(this, pieceData);
        return piece;
    }







    public bool IsCheck()
    {
        Tile king = myPieces[0].CurrentTile;
        return king.CanEnemyAttackToThis();
    }







    public bool IsMate()
    {
        Tile king = myPieces[0].CurrentTile;
        bool isCheck = IsCheck();

        if (isCheck)
        {
            if (CanGoAround(king) || CanAttackToDangerEnemy(king)) return false;
        }
        else return false;

        return true;
    }







    bool CanGoAround(Tile king)
    {
        List<Tile> kingPath = king.GetCanGoToAround();

        for (int i = 0; i < kingPath.Count; i++)
        {
            if (!kingPath[i].CanEnemyAttackToThis())
            {
                if (!kingPath[i].CanEnemyAttackToThis()) return true;
            }
        }

        return false;
    }







    bool CanAttackToDangerEnemy(Tile king)
    {
        List<Piece> enemiesDanger = king.currentPiece.EnemiesCanAttackToSelf();

        if (enemiesDanger.Count > 1) return false;
        List<Piece> piecesCanAttackToDangerEnemy = PiecesCanAttackToEnemy(enemiesDanger[0].CurrentTile);

        if (CanDefenseWithPieces(enemiesDanger[0])) return true;

        if (piecesCanAttackToDangerEnemy.Count == 0) return false;
        else if (piecesCanAttackToDangerEnemy.Count == 1 && piecesCanAttackToDangerEnemy[0] != king)
        {
            if (enemiesDanger[0].CurrentTile.CanEnemyAttackToThis()) return false;
        }

        return true;
    }







    public List<Piece> PiecesCanAttackToEnemy(Tile enemy)
    {
        List<Piece> myPieces = GameManager.instance.allPlayers[TurnSystem.instance.Turn].myPieces;
        List<Piece> tiles = new List<Piece>();

        for (int i = 0; i < myPieces.Count; i++)
        {
            List<Tile> myPiecePaths = myPieces[i].GetPiecePathsToMove();
            for (int j = 0; j < myPiecePaths.Count; j++)
            {
                if (myPiecePaths[j] == enemy) tiles.Add(myPiecePaths[j].currentPiece);
            }
        }

        return tiles;
    }







    bool CanDefenseWithPieces(Piece enemy)
    {
        List<Piece> myPieces = GameManager.instance.allPlayers[TurnSystem.instance.Turn].myPieces;
        Piece king = myPieces[0];
        List<Tile> pathsToCheck = enemy.GetCheckPath(king.CurrentTile, enemy.CurrentTile);

        for (int i = 0; i < pathsToCheck.Count; i++)
        {
            List<Piece> defensePieces = PiecesCanMoveToTile(pathsToCheck[i]);
            for (int j = 0; j < defensePieces.Count; j++)
            {
                if (defensePieces[j].data.Type != PieceType.King) return true;
            }
        }

        return false;
    }







    List<Piece> PiecesCanMoveToTile(Tile targetTile)
    {
        List<Piece> myPieces = GameManager.instance.allPlayers[TurnSystem.instance.Turn].myPieces;
        List<Piece> pieces = new List<Piece>();

        for (int i = 0; i < myPieces.Count; i++)
        {
            List<List<Tile>> piecePath = myPieces[i].CurrentTile.PathsCanMove(myPieces[i]);
            if (ChessBoard.instance.ContainTile(piecePath, targetTile))
            {
                if (myPieces[i].data.Type != PieceType.King) pieces.Add(myPieces[i]);
            }
        }

        return pieces;
    }





}







[Serializable]
public class PlayerData
{
    public int playerID;
    public ColorType colorType;
}




