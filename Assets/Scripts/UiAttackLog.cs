using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiAttackLog : MonoBehaviour
{
    public UiAttackLogData data;
    public ScrollRect attackLogRect;
    Font font;








    public void Init()
    {
        attackLogRect = GetComponent<ScrollRect>();
        font = Resources.Load("LiberationSans") as Font;
    }





    Text SetNewUiTextObj(string text)
    {
        Text temp = new GameObject("txtLog").AddComponent<Text>();
        temp.transform.SetParent(attackLogRect.content.transform, false);
        temp.fontSize = 11;
        temp.font = font;
        temp.alignment = TextAnchor.MiddleLeft;
        temp.color = Color.white;
        temp.text = text;
        return temp;
    }





    public void NewLog(string text)
    {
        data.logs.Enqueue(text);
        Refresh();
    }





    public void Refresh()
    {
        ClearAllUiTexts();
        for (int i = 0; i < data.logs.Count; i++)
        {
            Text txt = SetNewUiTextObj(data.logs.ToArray()[i]);
        }
        attackLogRect.DOVerticalNormalizedPos(0, 0.7f, false);
    }





    public void ClearAllUiTexts()
    {
        for (int i = 0; i < attackLogRect.content.childCount; i++) Destroy(attackLogRect.content.GetChild(i).gameObject);
    }





}





[Serializable]
public class UiAttackLogData
{
    public Queue<string> logs = new Queue<string>();
    public UiAttackLogData() { }

}


