using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public class Piece : MonoBehaviour
{
    public SinglePieceData data = new SinglePieceData();
    public string CurrentPosition;
    public Tile CurrentTile;
    public Player myPlayer;
    public bool isMyTurn => TurnSystem.instance.Turn == myPlayer.data.playerID;
    public bool doesFirstMoved => transform.position != defaultPos;
    public bool isAchmazPin;
    SpriteRenderer myRenderer;
    Vector3 defaultPos;







    public bool CheckPawnInLastTile()
    {
        if (data.Type == PieceType.Pawn)
        {
            if (myPlayer.data.playerID == 0 && CurrentTile.name[1] == '7') return true; // check white player
            if (myPlayer.data.playerID == 1 && CurrentTile.name[1] == '2') return true; // check white player
        }

        return false;
    }






    public List<Tile> GetPiecePathsToMove()
    {
        if (data.Type == PieceType.Pawn) return PawnPiecePathMove();

        List<List<Tile>> allPaths = CurrentTile.PathsCanMove(this);
        List<Tile> selectedPaths = new List<Tile>();

        for (int i = 0; i < allPaths.Count; i++)
        {
            for (int j = 0; j < allPaths[i].Count; j++)
            {
                if (allPaths[i][j] == null) continue;
                selectedPaths.Add(allPaths[i][j]);
            }
        }

        return selectedPaths;
    }







    List<Tile> PawnPiecePathMove()
    {
        List<Tile> selectedPaths = new List<Tile>();
        List<List<string>> pathToAttack = PathToMove(TurnSystem.instance.Turn);

        for (int i = 0; i < 2 /* pawn can two side attack! */; i++) selectedPaths.Add(ChessBoard.instance.GetTileByName(pathToAttack[i][0]));

        return selectedPaths;
    }







    public List<Tile> PathToTileMove(CellDirection dir, int playerid)
    {
        List<Tile> pathsTile = new List<Tile>();
        Tile nextTile = CurrentTile.aroundsTiles.GetAroundTileFromDirection(dir);

        for (int i = 0; i < ChessBoard.instance.data.Tile_Count; i++)
        {
            if (nextTile == null) break;
            else
            {
                if (nextTile.isEmpty)
                {
                    pathsTile.Add(nextTile);
                    nextTile = nextTile.aroundsTiles.GetAroundTileFromDirection(dir);
                }
                else
                {
                    //if (nextTile.currentPiece.myPlayer.data.playerID != playerid)
                    pathsTile.Add(nextTile);
                    break;
                }
            }
        }
        return pathsTile;
    }







    public List<List<string>> PathToMove(int playerid)
    {
        List<List<string>> paths = new List<List<string>>();
        int Char = CurrentTile.name.Substring(0, 1).toDigit();
        int Number = int.Parse(CurrentTile.name.Substring(1, 1));
        List<Tile> temp = new List<Tile>();

        switch (data.Type)
        {
            case PieceType.King:
                // Plus Move
                paths.Add(new List<string>() { $"{(Char).toChar()}{Number + 1}" });
                paths.Add(new List<string>() { $"{(Char).toChar()}{Number - 1}" });
                paths.Add(new List<string>() { $"{(Char + 1).toChar()}{Number}" });
                paths.Add(new List<string>() { $"{(Char - 1).toChar()}{Number}" });
                // Cross Move
                paths.Add(new List<string>() { $"{(Char + 1).toChar()}{Number + 1}" });
                paths.Add(new List<string>() { $"{(Char - 1).toChar()}{Number - 1}" });
                paths.Add(new List<string>() { $"{(Char + 1).toChar()}{Number - 1}" });
                paths.Add(new List<string>() { $"{(Char - 1).toChar()}{Number + 1}" });
                break;

            case PieceType.Queen:
                // Plus Move
                temp = PathToTileMove(CellDirection.Left, playerid); paths.Add(ChessBoard.instance.NameToTile(temp)); temp.Clear();
                temp = PathToTileMove(CellDirection.Right, playerid); paths.Add(ChessBoard.instance.NameToTile(temp)); temp.Clear();
                temp = PathToTileMove(CellDirection.Top, playerid); paths.Add(ChessBoard.instance.NameToTile(temp)); temp.Clear();
                temp = PathToTileMove(CellDirection.Down, playerid); paths.Add(ChessBoard.instance.NameToTile(temp)); temp.Clear();
                // Cross Move
                temp = PathToTileMove(CellDirection.TopLeft, playerid); paths.Add(ChessBoard.instance.NameToTile(temp)); temp.Clear();
                temp = PathToTileMove(CellDirection.TopRight, playerid); paths.Add(ChessBoard.instance.NameToTile(temp)); temp.Clear();
                temp = PathToTileMove(CellDirection.DownLeft, playerid); paths.Add(ChessBoard.instance.NameToTile(temp)); temp.Clear();
                temp = PathToTileMove(CellDirection.DownRight, playerid); paths.Add(ChessBoard.instance.NameToTile(temp)); temp.Clear();
                break;

            case PieceType.Rook:
                temp = PathToTileMove(CellDirection.Left, playerid); for (int i = 0; i < temp.Count; i++) paths.Add(ChessBoard.instance.NameToTile(temp)); temp.Clear();
                temp = PathToTileMove(CellDirection.Right, playerid); for (int i = 0; i < temp.Count; i++) paths.Add(ChessBoard.instance.NameToTile(temp)); temp.Clear();
                temp = PathToTileMove(CellDirection.Top, playerid); for (int i = 0; i < temp.Count; i++) paths.Add(ChessBoard.instance.NameToTile(temp)); temp.Clear();
                temp = PathToTileMove(CellDirection.Down, playerid); for (int i = 0; i < temp.Count; i++) paths.Add(ChessBoard.instance.NameToTile(temp)); temp.Clear();
                break;

            case PieceType.Bishop:
                temp = PathToTileMove(CellDirection.TopLeft, playerid); for (int i = 0; i < temp.Count; i++) paths.Add(ChessBoard.instance.NameToTile(temp)); temp.Clear();
                temp = PathToTileMove(CellDirection.TopRight, playerid); for (int i = 0; i < temp.Count; i++) paths.Add(ChessBoard.instance.NameToTile(temp)); temp.Clear();
                temp = PathToTileMove(CellDirection.DownLeft, playerid); for (int i = 0; i < temp.Count; i++) paths.Add(ChessBoard.instance.NameToTile(temp)); temp.Clear();
                temp = PathToTileMove(CellDirection.DownRight, playerid); for (int i = 0; i < temp.Count; i++) paths.Add(ChessBoard.instance.NameToTile(temp)); temp.Clear();
                break;

            case PieceType.Knight:
                paths.Add(new List<string>() { $"{(Char - 1).toChar()}{Number + 2}" });
                paths.Add(new List<string>() { $"{(Char + 1).toChar()}{Number + 2}" });

                paths.Add(new List<string>() { $"{(Char - 1).toChar()}{Number - 2}" });
                paths.Add(new List<string>() { $"{(Char + 1).toChar()}{Number - 2}" });

                paths.Add(new List<string>() { $"{(Char + 2).toChar()}{Number - 1}" });
                paths.Add(new List<string>() { $"{(Char + 2).toChar()}{Number + 1}" });

                paths.Add(new List<string>() { $"{(Char - 2).toChar()}{Number - 1}" });
                paths.Add(new List<string>() { $"{(Char - 2).toChar()}{Number + 1}" });
                break;

            case PieceType.Pawn:
                if (myPlayer.data.playerID == 0) // is white player
                {
                    paths.Add(new List<string>() { $"{(Char + 1).toChar()}{Number + 1}" }); // attack move
                    paths.Add(new List<string>() { $"{(Char - 1).toChar()}{Number + 1}" }); // attack move
                    paths.Add(new List<string>() { $"{Char.toChar()}{Number + 1}" });
                    if (!doesFirstMoved) paths.Add(new List<string>() { $"{Char.toChar()}{Number + 2}" }); // first two move
                }
                else // is black player
                {
                    paths.Add(new List<string>() { $"{(Char + 1).toChar()}{Number - 1}" }); // attack move
                    paths.Add(new List<string>() { $"{(Char - 1).toChar()}{Number - 1}" }); // attack move
                    paths.Add(new List<string>() { $"{Char.toChar()}{Number - 1}" });
                    if (!doesFirstMoved) paths.Add(new List<string>() { $"{Char.toChar()}{Number - 2}" }); // first two move
                }
                break;
        }

        return paths;
    }







    public void Init(Player Player, SinglePieceData data)
    {
        myPlayer = Player;
        this.data.Clone(data);
        transform.parent = Player.transform;
        BoxCollider2D boxCollider2D = gameObject.AddComponent<BoxCollider2D>();
        boxCollider2D.isTrigger = true;
        boxCollider2D.size = new Vector2(transform.localScale.x / 2, transform.localScale.y / 2);
        myRenderer = gameObject.AddComponent<SpriteRenderer>();
        myRenderer.sortingOrder = 1;
        gameObject.tag = "Piece";
        CurrentPosition = data.DefaultPosition;
        this.data.PlayerID = myPlayer.data.playerID;
        if (myPlayer.data.playerID == 0) CurrentTile = ChessBoard.instance.GetTileByName(data.DefaultPosition);
        else CurrentTile = ChessBoard.instance.GetReverseTileByName(data.DefaultPosition);
        CurrentTile.SyncToPiece(this);
        transform.position = ChessBoard.instance.GetPositionByName(CurrentPosition, (myPlayer.data.playerID > 0));
        defaultPos = transform.position;
        Refresh();
    }







    public void Refresh()
    {
        name = data.Type.ToString();
        myRenderer.sprite = ResourceManager.instance.GetSpriteByTypeID(data.Type, myPlayer.data.playerID);
    }









    public void CheckMoveToReservedTile(Tile sender)
    {
        if (CheckPawnInLastTile()) ChooseNewPieceToReplacePawn(() => { MoveToReservedTile(sender); });
        else MoveToReservedTile(sender);
    }






    public void MoveToReservedTile(Tile sender)
    {
        ChessBoard.instance.selectedPiece.CurrentTile.currentPiece = null;
        CurrentTile.isSelected = false;
        ChessBoard.instance.RefreshAll();
        sender.SyncToPiece(this);
        CurrentTile = sender;
        transform.position = new Vector3(sender.transform.position.x, sender.transform.position.y, 0);
        TurnSystem.instance.PlayNext();
        GameManager.instance.PlayerCheckingMate();
    }







    public List<Piece> EnemiesCanAttackToSelf()
    {
        List<Piece> enemy = GameManager.instance.allPlayers[TurnSystem.instance.GetNextTurn()].myPieces;
        List<Piece> attack = new List<Piece>();

        for (int i = 0; i < enemy.Count; i++)
        {
            List<List<Tile>> enemyPathToMove = enemy[i].CurrentTile.PathsCanMove(enemy[i]);
            if (ChessBoard.instance.ContainTile(enemyPathToMove, CurrentTile)) attack.Add(enemy[i]);
        }

        return attack;
    }







    public List<Tile> GetCheckPath(Tile king, Tile enemy)
    {
        List<List<Tile>> enemyPath = enemy.PathsCanMove(enemy.currentPiece);

        for (int i = 0; i < enemyPath.Count; i++)
        {
            for (int j = 0; j < enemyPath[i].Count; j++)
            {
                if (enemyPath[i][j] == king)
                {
                    enemyPath[i].Remove(king);
                    return enemyPath[i];
                }
            }
        }

        return null;
    }





    public void ChooseNewPieceToReplacePawn(UnityAction callback)
    {
        int currentID = myPlayer.data.playerID;
        Dictionary<Sprite, UnityAction> dict = new Dictionary<Sprite, UnityAction>()
        {
            {ResourceManager.instance.GetSpriteByTypeID(PieceType.Queen, currentID), ()=>{ data.Type = PieceType.Queen; Refresh(); callback.Invoke(); } },
            {ResourceManager.instance.GetSpriteByTypeID(PieceType.Knight, currentID), ()=>{ data.Type = PieceType.Knight; Refresh(); callback.Invoke();} },
            {ResourceManager.instance.GetSpriteByTypeID(PieceType.Rook, currentID), ()=>{ data.Type = PieceType.Rook; Refresh(); callback.Invoke();} },
            {ResourceManager.instance.GetSpriteByTypeID(PieceType.Bishop, currentID), ()=>{ data.Type = PieceType.Bishop; Refresh(); callback.Invoke();} }
        };
        UiMessageBox.instance.ShowWithSprite(dict, new Vector2(55, 85), true);

    }





}







[Serializable]
public class SinglePieceData
{
    public PieceType Type;
    public int Score;
    public int PlayerID;
    public string DefaultPosition;



    public SinglePieceData() { }
    public SinglePieceData(JSONNode node, string pos)
    {
        Type = (PieceType)Enum.Parse(typeof(PieceType), node["type"].Value);
        Score = node["score"].AsInt;
        DefaultPosition = pos;
    }


    public void Clone(SinglePieceData cloneData)
    {
        Type = cloneData.Type;
        Score = cloneData.Score;
        PlayerID = cloneData.PlayerID;
        DefaultPosition = cloneData.DefaultPosition;
    }

}


