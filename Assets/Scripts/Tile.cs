using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public enum CellDirection { Top = 0, TopRight = 1, Right = 2, DownRight = 3, Down = 4, DownLeft = 5, Left = 6, TopLeft = 7 }

public class Tile : MonoBehaviour
{
    public ColorType Type;
    public AroundTiles aroundsTiles = new AroundTiles();
    public Piece currentPiece;
    public bool isEmpty => currentPiece == null;
    public bool isSelected;
    public bool isReservedToMoveHere;
    public bool isDanger;
    SpriteRenderer render;









    public void DetectAllPath(Piece sender)
    {
        List<List<Tile>> temps = PathsCanMove(sender);
        for (int i = 0; i < temps.Count; i++)
        {
            for (int j = 0; j < temps[i].Count; j++) ChessBoard.instance.SetColorToTile(sender, temps[i][j]);
        }
    }







    public List<List<Tile>> PathsCanMove(Piece sender)
    {
        List<List<string>> allPath = sender.PathToMove(sender.myPlayer.data.playerID);
        List<List<Tile>> tempPath = new List<List<Tile>>();

        for (int i = 0; i < allPath.Count; i++)
        {
            if (sender.data.Type == PieceType.Pawn)
            {
                if(!PawnPathsCanMove(i, allPath, tempPath)) return tempPath;
            }
            else tempPath.Add(ChessBoard.instance.TileToName(allPath[i]));
        }

        return tempPath;
    }







    bool PawnPathsCanMove(int i, List<List<string>> allPath, List<List<Tile>> tempPath)
    {
        Tile tempTargetTile = ChessBoard.instance.GetTileByName(allPath[i][0]);

        if (tempTargetTile != null)
        {
            if (!tempTargetTile.isEmpty && i == 2) return false; // if topleft and topright and top not empty
            else if (tempTargetTile.isEmpty && i == 2) // if topleft and topright and top was empty
                tempPath.Add(new List<Tile>() { ChessBoard.instance.GetTileByName(allPath[i][0]) });

            if (!tempTargetTile.isEmpty && i < 2) tempPath.Add(new List<Tile>() { ChessBoard.instance.GetTileByName(allPath[i][0]) });
            if (tempTargetTile.isEmpty && i == 3) tempPath.Add(new List<Tile>() { ChessBoard.instance.GetTileByName(allPath[i][0]) });
        }
        return true;
    }







    public void GetAllConnectedTiles()
    {
        for (int i = 0; i < Enum.GetNames(typeof(CellDirection)).Length; i++)
        {
            CellDirection dir = (CellDirection)i;

            string temp = GetConnectedCell(dir);
            if (temp != "END")
            {
                aroundsTiles.Add(dir, ChessBoard.instance.GetTileByName(temp));
            }
            else aroundsTiles.Add(dir, null);
        }
    }







    string GetConnectedCell(CellDirection dir)
    {
        int tilesCount = ChessBoard.instance.data.Tile_Count;
        int Char = name.Substring(0, 1).toDigit();
        int Number = int.Parse(name.Substring(1, 1));
        string End = "END";


        switch (dir)
        {
            case CellDirection.Top:
                if (Number >= tilesCount) return End;
                return Char.toChar() + (Number + 1).ToString();

            case CellDirection.Down:
                if (Number <= 1) return End;
                return Char.toChar() + (Number - 1).ToString();

            case CellDirection.Left:
                if (Char < 1) return End;
                return (Char - 1).toChar() + Number.ToString();

            case CellDirection.Right:
                if (Char >= tilesCount - 1) return End;
                return (Char + 1).toChar() + Number.ToString();

            case CellDirection.TopLeft:
                if (Number >= tilesCount || Char < 1) return End;
                return (Char - 1).toChar() + (Number + 1).ToString();

            case CellDirection.TopRight:
                if (Number >= tilesCount || Char >= tilesCount - 1) return End;
                return (Char + 1).toChar() + (Number + 1).ToString();

            case CellDirection.DownLeft:
                if (Number <= 1 || Char < 1) return End;
                return (Char - 1).toChar() + (Number - 1).ToString();

            case CellDirection.DownRight:
                if (Number <= 1 || Char >= tilesCount - 1) return End;
                return (Char + 1).toChar() + (Number - 1).ToString();
        }

        return null;
    }







    public void Init(ColorType type, int x, int y, float tileSize)
    {
        Type = type;
        render = gameObject.AddComponent<SpriteRenderer>();
        render.sprite = GameManager.instance.board.sprTile;
        BoxCollider2D collider = gameObject.AddComponent<BoxCollider2D>();
        collider.isTrigger = true;
        collider.size = new Vector2(tileSize, tileSize);
        transform.position = new Vector3(x * tileSize, y * tileSize, -1);
        transform.localScale = new Vector2(tileSize, tileSize);
        gameObject.layer = LayerMask.NameToLayer("Tile");
        Rigidbody2D rb = gameObject.AddComponent<Rigidbody2D>();
        rb.gravityScale = 0;
        gameObject.tag = "Tile";
        GameManager.instance.AllLoaded.AddListener(GetAllConnectedTiles);
        Refresh();
    }







    public void Refresh()
    {
        isSelected = false;
        isReservedToMoveHere = false;
        isDanger = false;

        switch (Type)
        {
            case ColorType.Black: render.color = DataManager.instance.Col_Black; break;
            case ColorType.White: render.color = DataManager.instance.Col_White; break;
        }
    }







    public List<Tile> GetCanGoToAround()
    {
        List<Tile> tiles = new List<Tile>();
        List<List<Tile>> canMovepaths = PathsCanMove(currentPiece);

        for (int i = 0; i < canMovepaths.Count; i++)
        {
            for (int j = 0; j < canMovepaths[i].Count; j++)
            {
                Tile tempTargetTile = canMovepaths[i][j];
                if (tempTargetTile != null)
                {
                    if (tempTargetTile.isEmpty) tiles.Add(tempTargetTile);
                    else
                    {
                        if (tempTargetTile.currentPiece.myPlayer.data.playerID != currentPiece.myPlayer.data.playerID) // opponent!
                        {
                            if (tempTargetTile.currentPiece.data.Type != PieceType.King) tiles.Add(tempTargetTile);
                        }
                    }
                }
            }
        }

        return tiles;
    }







    public bool CanEnemyAttackToThis()
    {
        List<Piece> enemies = GameManager.instance.allPlayers[TurnSystem.instance.GetNextTurn()].myPieces;

        for (int i = 0; i < enemies.Count; i++)
        {
            List<Tile> enemyCanMoveToTiles = enemies[i].GetPiecePathsToMove();
            for (int j = 0; j < enemyCanMoveToTiles.Count; j++)
            {
                if (enemyCanMoveToTiles[j] == this) return true;
            }
        }

        return false;
    }







    public void Clicked()
    {
        render.color = DataManager.instance.Col_Touched;

        if (!isEmpty)
        {
            if (currentPiece.isMyTurn) PlayMyPieceTurn();
            else
            {
                if (ChessBoard.instance.selectedPiece != null && currentPiece.data.Type != PieceType.King) AttackEnemyToSelf();
            }
            if (isReservedToMoveHere && currentPiece.data.Type != PieceType.King) ChessBoard.instance.selectedPiece.CheckMoveToReservedTile(this);
        }
        else
        {
            if (isReservedToMoveHere) ChessBoard.instance.selectedPiece.CheckMoveToReservedTile(this);
        }
    }







    void PlayMyPieceTurn()
    {
        ChessBoard.instance.RefreshAll();
        isSelected = true;
        DetectAllPath(currentPiece);
        ChessBoard.instance.selectedPiece = currentPiece;
    }







    void AttackEnemyToSelf()
    {
        Piece Attacker = ChessBoard.instance.selectedPiece;
        Piece Defender = currentPiece;

        if (Defender.CurrentTile.isDanger)
        {
            string log = $"{Attacker.myPlayer.data.colorType} {Attacker.data.Type} => " +
                $"{Defender.myPlayer.data.colorType} {Defender.data.Type}";

            UiCTRL.instance.uiAttackLog.NewLog(log);
            Recycle.instance.AddToRecycle(Defender);
            Defender.myPlayer.myPieces.Remove(Defender);
        }
    }







    public void Move(Tile target)
    {
        transform.position = target.transform.position;
        if (target.currentPiece != null) target.AttackEnemyToSelf();
    }







    public void SyncToPiece(Piece sender)
    {
        currentPiece = sender;
    }







    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> [Events] <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<





    void OnMouseExit()
    {
        if (GameManager.instance.canPlay)
        {
            if (!isSelected && !isReservedToMoveHere) Refresh();
            else if (!isReservedToMoveHere) render.color = DataManager.instance.Col_Selected;
        }
    }





    void OnMouseDown()
    {
        if (GameManager.instance.canPlay) Clicked();
    }





    void OnMouseUp()
    {
        if (!isSelected && GameManager.instance.canPlay) Refresh();
    }





    void OnMouseOver()
    {
        if (GameManager.instance.canPlay)
        {
            if (ChessBoard.instance.IsTouched) render.color = DataManager.instance.Col_Touched;
            else if (!isReservedToMoveHere) render.color = DataManager.instance.Col_TouchOver;
        }
    }





    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> [End Events] <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



}


