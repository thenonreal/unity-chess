using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System;
using UnityEngine.Events;
using System.Threading.Tasks;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public DataManager dataManager;
    public ChessBoard board;
    public TurnSystem turnSystem;
    public UiCTRL uiCTRL;
    public List<Player> allPlayers = new List<Player>();
    public UnityEvent AllLoaded;
    public bool canPlay;
    [SerializeField] TextAsset jsonText;







    void Awake() => instance = this;





    void Start()
    {
        Init();
        AllLoaded.Invoke();
    }







    void Init()
    {
        dataManager?.init(JSONNode.Parse(jsonText.text));
        board?.Init(dataManager.metaData.TileData);
        GeneratePlayers(dataManager.metaData.PiecesData, dataManager.metaData.MaxPlayer);
        turnSystem.Init(allPlayers.Count);
        uiCTRL?.Init();
    }







    void GeneratePlayers(List<PiecesData> piecesData, int maxPlayer)
    {
        for (int i = 0; i < maxPlayer; i++)
        {
            Player tempPlayer = InstantiateNewPlayer(i);
            allPlayers.Add(tempPlayer);
        }
    }







    Player InstantiateNewPlayer(int id)
    {
        Player player = new GameObject($"Player{id}").AddComponent<Player>();
        player.Init(id);
        return player;
    }







    public void PlayerCheckingMate()
    {
        Player currentPlayer = allPlayers[TurnSystem.instance.Turn];

        // if player check & mate
        if (currentPlayer.IsMate())
        {
            Debug.Log($"{ChessBoard.instance.GetPlayerNameById(currentPlayer.data.playerID)} is mate!");
            canPlay = false;

            Dictionary<string, UnityAction> dict = new Dictionary<string, UnityAction>()
            {
                {"Restart", ()=>{ SceneManager.LoadScene(0); } },
                {"Quit", ()=>{ Application.Quit(); } }
            };

            string winnerPlayer = (currentPlayer.data.playerID == 0) ? "Black" : "White";
            UiMessageBox.instance.ShowWithText(dict, $"{winnerPlayer} Win!", false);
            return;
        }

        // if player check
        if (currentPlayer.IsCheck()) Debug.Log($"{ChessBoard.instance.GetPlayerNameById(currentPlayer.data.playerID)} is check!");

    }



}









public static class ExtensionMethods
{

    public static char toChar(this int digit)
    {
        return (char)(digit + 65);
    }



    public static int toDigit(this string digit)
    {
        digit = digit.Substring(0, 1);
        char myChar = char.Parse(digit);
        int temp = myChar;
        return (char)(temp - 65);
    }



}






