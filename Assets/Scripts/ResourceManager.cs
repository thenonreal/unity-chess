using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class ResourceManager : MonoBehaviour
{
    public static ResourceManager instance;
    public Sprite[] blackPieces;
    public Sprite[] whitePieces;







    void Awake() => instance = this;





    public void init()
    {

    }





    public Sprite GetSpriteByTypeID(PieceType type, int PlayerID)
    {
        Sprite[] sprites = PlayerID == 0 ? whitePieces : blackPieces;

        switch(type)
        {
            case PieceType.King: return sprites[0];
            case PieceType.Queen: return sprites[1];
            case PieceType.Rook: return sprites[2];
            case PieceType.Bishop: return sprites[3];
            case PieceType.Knight: return sprites[4];
            case PieceType.Pawn: return sprites[5];
        }

        return null;
    }





}


