using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

enum MessageType { TextAndButton, ImageAndButton }

public class UiMessageBox : MonoBehaviour
{
    public static UiMessageBox instance;
    public Transform textButtonObj, imageButtonObj;
    GridLayoutGroup gridLayoutGroup;
    Text txtBody;
    Font font;
    Sprite sprButton;
    Button btnClose;









    public void Init()
    {
        instance = this;
        font = Resources.Load("LiberationSans") as Font;
        sprButton = Resources.Load<Sprite>("Tile");
        btnClose = transform.GetChild(0).transform.GetChild(0).transform.GetChild(0).GetComponent<Button>();
        txtBody = transform.GetChild(0).transform.GetChild(0).transform.GetChild(1).GetComponent<Text>();
        textButtonObj = transform.GetChild(0).transform.GetChild(0).transform.GetChild(2);
        imageButtonObj = transform.GetChild(0).transform.GetChild(0).transform.GetChild(3);
        gridLayoutGroup = imageButtonObj.GetComponent<GridLayoutGroup>();
    }







    public void ShowWithText(Dictionary<string, UnityAction> dict, string bodyText, bool canCLose)
    {
        GameManager.instance.canPlay = false;
        btnClose.gameObject.SetActive(canCLose);
        gameObject.SetActive(true);
        ResetModes();
        ShowTextButton(dict, bodyText, canCLose);
    }







    public void ShowWithSprite(Dictionary<Sprite, UnityAction> dict, Vector2 buttonSize, bool canCLose)
    {
        GameManager.instance.canPlay = false;
        btnClose.gameObject.SetActive(canCLose);
        gameObject.SetActive(true);
        gridLayoutGroup.cellSize = buttonSize;
        ResetModes();
        ShowImageButton(dict);
    }







    void ShowTextButton(Dictionary<string, UnityAction> dict, string bodyText, bool canCLose)
    {
        textButtonObj.gameObject.SetActive(true);
        txtBody.text = bodyText;

        for (int i = 0; i < dict.Count; i++)
        {
            Button btnTemp = SetImageButton(MessageType.TextAndButton);
            Text btnText = btnTemp.transform.GetChild(0).GetComponent<Text>();
            btnTemp.onClick.AddListener(dict.ElementAt(i).Value);
            btnText.text = dict.ElementAt(i).Key.ToString();
        }
    }







    void ShowImageButton(Dictionary<Sprite, UnityAction> dict)
    {
        imageButtonObj.gameObject.SetActive(true);

        for (int i = 0; i < dict.Count; i++)
        {
            Button btnTemp = SetImageButton(MessageType.ImageAndButton);
            Image btnImg = btnTemp.GetComponent<Image>();
            btnImg.sprite = dict.ElementAt(i).Key;
            btnTemp.onClick.AddListener(dict.ElementAt(i).Value);
        }
    }







    void ResetModes()
    {
        ClearAll();
        txtBody.text = string.Empty;
        textButtonObj.gameObject.SetActive(false);
        imageButtonObj.gameObject.SetActive(false);
    }







    void ClearAll()
    {
        for (int i = 0; i < textButtonObj.childCount; i++) Destroy(textButtonObj.GetChild(i).gameObject);
        for (int i = 0; i < imageButtonObj.childCount; i++) Destroy(imageButtonObj.GetChild(i).gameObject);
    }







    Button SetImageButton(MessageType type)
    {
        Button btnTemp = new GameObject().AddComponent<Button>();
        Image sprite = btnTemp.AddComponent<Image>();
        Text txt = new GameObject("text").AddComponent<Text>();
        btnTemp.onClick.AddListener(() => Hide());
        sprite.sprite = sprButton;
        txt.transform.SetParent(btnTemp.transform, false);
        txt.font = font;
        txt.fontSize = 15;
        txt.alignment = TextAnchor.MiddleCenter;
        txt.color = Color.black;
        switch (type)
        {
            case MessageType.TextAndButton: btnTemp.transform.SetParent(textButtonObj, false); break;
            case MessageType.ImageAndButton: btnTemp.transform.SetParent(imageButtonObj, false); break;
        }
        return btnTemp;
    }







    public void Hide()
    {
        GameManager.instance.canPlay = true;
        gameObject.SetActive(false);
    }





}


