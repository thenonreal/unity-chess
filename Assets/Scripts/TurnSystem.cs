using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnSystem : MonoBehaviour
{
    public static TurnSystem instance;
    public int allPlayer;
    public int Turn = 0;





    void Awake() => instance = this;





    public void Init(int playersCount)
    {
        allPlayer = playersCount;
    }





    public void PlayNext()
    {
        Turn = (Turn >= allPlayer - 1) ? 0 : Turn + 1;
    }





    public int GetNextTurn()
    {
        return (Turn >= allPlayer - 1) ? 0 : Turn + 1;
    }





    public int GetDifferentPlayer(int currentPlayerID)
    {
        return (currentPlayerID >= allPlayer - 1) ? 0 : currentPlayerID + 1;
    }


}


