using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System;
using TMPro;

public class ChessBoard : MonoBehaviour
{
    public static ChessBoard instance;
    public ChessBoardData data;
    public Sprite sprTile;
    public bool IsTouched => Input.GetMouseButton(0);
    public Piece selectedPiece;
    public Tile[,] Tiles;
    Transform Texts;









    void Awake() => instance = this;





    public void Init(ChessBoardData data)
    {
        this.data = data;
        GererateAllTile();
    }







    public void SetColorToTile(Piece sender, Tile targetTile)
    {
        Tile tempTargetTile = targetTile;
        if (tempTargetTile != null)
        {
            SpriteRenderer tileRenderer = tempTargetTile.GetComponent<SpriteRenderer>();
            if (tempTargetTile.isEmpty)
            {
                tileRenderer.color = DataManager.instance.Col_Reserved;
                selectedPiece = sender;
            }
            else
            {
                if (tempTargetTile.currentPiece.myPlayer.data.playerID != sender.myPlayer.data.playerID) // opponent!
                {
                    if (tempTargetTile.currentPiece.data.Type != PieceType.King)
                    {
                        tileRenderer.color = DataManager.instance.Col_CanAttack;
                        tempTargetTile.isDanger = true;
                    }
                }
            }
            tempTargetTile.isReservedToMoveHere = true;
        }
    }







    public Tile GetTileByName(string name)
    {
        for (int i = 0; i < Tiles.GetLength(0); i++)
        {
            for (int j = 0; j < Tiles.GetLength(1); j++)
            {
                if (Tiles[i, j].name == name) return Tiles[i, j];
            }
        }
        return null;
    }







    public Tile GetReverseTileByName(string name)
    {
        for (int i = 0; i < Tiles.GetLength(0); i++)
        {
            for (int j = 0; j < Tiles.GetLength(1); j++)
            {
                int tempJ = ((data.Tile_Count - 1) - j);
                if (Tiles[i, j].name == name) return Tiles[i, tempJ];
            }
        }
        return null;
    }







    public Vector2 GetPositionByName(string name, bool reverse)
    {
        for (int i = 0; i < Tiles.GetLength(0); i++)
        {
            for (int j = 0; j < Tiles.GetLength(1); j++)
            {
                if (Tiles[i, j].name == name)
                {
                    if (reverse) j = ((data.Tile_Count - 1) - j);
                    return Tiles[i, j].transform.position;
                }
            }
        }
        return new Vector2(0, 0);
    }







    void GererateAllTile()
    {
        Texts = new GameObject("AllTexts").transform;
        Texts.transform.parent = transform;
        Tiles = new Tile[data.Tile_Count, data.Tile_Count];
        SetTiles();
        transform.position = new Vector2(-((data.Tile_Count - 1) / 2.0f * data.TileSize), -((data.Tile_Count - 1) / 2.0f * data.TileSize));
    }







    void SetTiles()
    {
        for (int x = 0; x < data.Tile_Count; x++)
        {
            TextMeshPro textH = GetText($"{x.toChar()}", new Vector2(x * data.TileSize, -.75f));
            TextMeshPro textV = GetText($"{x + 1}", new Vector2(-.75f, x * data.TileSize));
            for (int y = 0; y < data.Tile_Count; y++)
            {
                Tiles[x, y] = GenerateTile(x, y);
                if (y == data.Tile_Count - 1 && (data.Tile_Count % 2 == 0)) IsWhite = !IsWhite;
            }
        }
    }







    bool IsWhite = false;
    Tile GenerateTile(int x, int y)
    {
        GameObject tile = new GameObject($"{x.toChar()}{y + 1}");
        tile.transform.parent = transform;
        ColorType tempType = ColorType.Black;
        if (IsWhite) tempType = ColorType.White;
        tile.AddComponent<Tile>().Init(tempType, x, y, data.TileSize);
        IsWhite = !IsWhite;
        return tile.GetComponent<Tile>();
    }







    TextMeshPro GetText(string text, Vector2 pos)
    {
        GameObject obj = new GameObject("Text");
        obj.AddComponent<TextMeshPro>();
        TextMeshPro temp = obj.GetComponent<TextMeshPro>();
        temp.fontSize = 2.7f;
        temp.rectTransform.sizeDelta = new Vector2(0.7f, 0.7f);
        temp.alignment = TextAlignmentOptions.Center;
        temp.text = text;
        obj.transform.localPosition = pos;
        obj.transform.SetParent(Texts);
        return temp;
    }







    public void RefreshAll()
    {
        selectedPiece = null;
        for (int i = 0; i < Tiles.GetLength(0); i++)
        {
            for (int j = 0; j < Tiles.GetLength(1); j++)
            {
                Tiles[i, j].Refresh();
            }
        }
    }







    void Update()
    {
        if (Input.GetMouseButtonDown(1) && GameManager.instance.canPlay) RefreshAll();
    }







    public string GetPlayerNameById(int playerID)
    {
        string name = (playerID == 0) ? "White" : "Black";
        return name;
    }







    public List<string> NameToTile(List<Tile> tiles)
    {
        List<string> temp = new List<string>();
        for (int i = 0; i < tiles.Count; i++) temp.Add(tiles[i].name);
        return temp;
    }







    public List<Tile> TileToName(List<string> names)
    {
        List<Tile> temp = new List<Tile>();
        for (int i = 0; i < names.Count; i++) temp.Add(GetTileByName(names[i]));
        return temp;
    }







    public Tile ContainTile(List<List<Tile>> tiles, Tile target)
    {
        List<List<Tile>> temp = new List<List<Tile>>();

        for (int i = 0; i < tiles.Count; i++)
        {
            for (int j = 0; j < tiles[i].Count; j++)
            {
                if (tiles[i][j] == target) return tiles[i][j];
            }
        }

        return null;
    }





    public Tile GiveNextTile(Tile tile, bool upToDown)
    {
        char tileChar = tile.name[0];
        int tileNumber = int.Parse(tile.name[1].ToString());
        tileNumber = (upToDown) ? tileNumber - 1 : tileNumber + 1;
        return GetTileByName((tileChar + "" + tileNumber).ToString());
    }





}







[Serializable]
public class ChessBoardData
{
    [Range(0.1f, 1f)] public float TileSize;
    public int Tile_Count;

    public ChessBoardData() { }
    public ChessBoardData(float tileSize, int TileCount)
    {
        TileSize = tileSize;
        Tile_Count = TileCount;
    }
}








