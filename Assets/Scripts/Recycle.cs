using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Recycle : MonoBehaviour
{
    public static Recycle instance;
    public RecycleData data;







    void Awake() => instance = this;



    public void Init()
    {

    }







    public void AddToRecycle(Piece piece)
    {
        switch (piece.myPlayer.data.playerID)
        {
            case 0: data.whitePieces.Add(piece.data); break;
            case 1: data.blackPieces.Add(piece.data); break;
        }
        Destroy(piece.gameObject);
        Refresh();
    }







    public void Refresh()
    {
        UiCTRL.instance.ClearAllButtons();
        RefreshWhite();
        RefreshBlack();
    }







    void RefreshWhite()
    {
        for (int i = 0; i < data.whitePieces.Count; i++)
        {
            Button tempWhiteBtn = UiCTRL.instance.GetNewRecycleButton(data.whitePieces.ToArray()[i]);
        }
    }







    void RefreshBlack()
    {
        for (int i = 0; i < data.blackPieces.Count; i++)
        {
            Button tempBlackBtn = UiCTRL.instance.GetNewRecycleButton(data.blackPieces.ToArray()[i]);
        }
    }





}





[Serializable]
public class RecycleData
{
    public List<SinglePieceData> whitePieces = new List<SinglePieceData>();
    public List<SinglePieceData> blackPieces = new List<SinglePieceData>();



    public RecycleData() { }


}


