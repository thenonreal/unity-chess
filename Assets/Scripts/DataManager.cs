using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;


public enum PieceType { King = 0, Queen = 1, Rook = 2, Bishop = 3, Knight = 4, Pawn = 5 }
public enum ColorType { White = 0, Black = 1 }
public enum DifficultyLevel { None = 0, Amateur = 1, Medium = 2, Professional = 3 }



public class DataManager : MonoBehaviour
{
    public static DataManager instance;
    public Meta metaData;
    public User userData;
    public Color Col_White;
    public Color Col_Black;
    public Color Col_Touched;
    public Color Col_TouchOver;
    public Color Col_Selected;
    public Color Col_Reserved;
    public Color Col_CanAttack;









    void Awake() => instance = this;

    public void init(JSONNode node)
    {
        metaData = new Meta(node["meta"]);
        ResourceManager.instance.init();
    }



}





[Serializable]
public class PiecesData
{
    public List<SinglePieceData> Pieces = new List<SinglePieceData>();



    public PiecesData() { }
    public PiecesData(JSONNode node)
    {
        for (int i = 0; i < node["pos"].Count; i++) Pieces.Add(new SinglePieceData(node, node["pos"][i].Value));
    }


}





[Serializable]
public class AroundTiles
{
    public Tile Top;
    public Tile TopRight;
    public Tile Right;
    public Tile DownRight;
    public Tile Down;
    public Tile DownLeft;
    public Tile Left;
    public Tile TopLeft;

    public AroundTiles() { }
    
    public Tile GetAroundTileFromDirection(CellDirection dir)
    {
        if(dir == CellDirection.Top) return Top;
        if(dir == CellDirection.TopRight) return TopRight;
        if(dir == CellDirection.Right) return Right;
        if(dir == CellDirection.DownRight) return DownRight;
        if(dir == CellDirection.Down) return Down;
        if(dir == CellDirection.DownLeft) return DownLeft;
        if(dir == CellDirection.Left) return Left;
        return TopLeft;
    }
    


    public void Add(CellDirection dir, Tile cellName)
    
    {
        switch (dir)
        {
            case CellDirection.Top:
                Top = cellName;
                break;
            case CellDirection.TopRight:
                TopRight = cellName;
                break;
            case CellDirection.Right:
                Right = cellName;
                break;
            case CellDirection.DownRight:
                DownRight = cellName;
                break;
            case CellDirection.Down:
                Down = cellName;
                break;
            case CellDirection.DownLeft:
                DownLeft = cellName;
                break;
            case CellDirection.Left:
                Left = cellName;
                break;
            case CellDirection.TopLeft:
                TopLeft = cellName;
                break;
        }
    }
}






[Serializable]
public class Meta
{
    public ChessBoardData TileData;
    public List<PiecesData> PiecesData = new List<PiecesData>();
    public int MaxPlayer;





    public Meta() { }
    public Meta(JSONNode node)
    {
        TileData = new ChessBoardData(node["tile_size"].AsFloat, node["tile_count"].AsInt);
        MaxPlayer = node["max_player"].AsInt;

        for (int i = 0; i < node["pieces"].Count; i++)
        {
            PiecesData.Add(new PiecesData(node["pieces"][i]));
        }

    }

}





[Serializable]
public class User
{


}




